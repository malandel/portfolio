//Import the scss stylesheet that imports all the other stylesheets
import './assets/scss/main.scss';
//Import the bootstrap js file
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import './node_modules/devicon/devicon.css';
import './node_modules/bootstrap/dist/js/bootstrap.min.js';
//Import config infos for emailjs
import { emailjs_user_id } from './config.js';
import { emailjs_template_id } from './config.js';
import { emailjs_service_id } from './config.js';
import emailjs from 'emailjs-com';

//Init emailjs
(function() {
    emailjs.init(emailjs_user_id);
})();
//Function for emailjs when submitting the form
window.onload = function() {
    document.getElementById('contactForm').addEventListener('submit', function(event) {
        //Prevent the submit button from reloading the page
        event.preventDefault();
        // generate a five digit number for the contact_number variable
        this.contact_number.value = Math.random() * 100000 | 0;
        // these IDs from the previous steps
        emailjs.sendForm(emailjs_service_id, emailjs_template_id, '#contactForm')
            .then(function() {
                let message = document.getElementById('messageSendForm');
                message.innerHTML = "Thanks for your message ! "
            }, function(error) {
                message.innerHTML = `I'm sorry there was an error : ${error}`;
                console.log('FAILED...', error);
            });
    });
}