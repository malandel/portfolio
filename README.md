# Marie Guerin's portfolio || Full Stack Web Developer

## Online version
https://portfolio-marie-guerin.surge.sh/

## Online link to Figma (wireframe) - PDF in the 'wireframes' folder
https://www.figma.com/file/P3KCYxAnqaYY8pjhmXSKUJ/PORTFOLIO?node-id=1%3A2 

## How to run the project

- Clone this project

```
cd portfolio/

//Install dependencies :
npm install

//Now run Parceljs on watch mode:
npm run start

//Build your project with parcel (optimization):
npx parcel build app.js

```

- Then to see your project on a local server open http://localhost:1234

## Put it online with surge

```
cd dist/
surge --domain https://myDomainName.surge.sh

```
- Make sure you're in the dist folder and press enter
- Replace "myDomainName" by the name you have choosen for this project

## Devicon

- I'm using Devicon for the icons : 
https://github.com/devicons/devicon/

## Emailjs

- I'm using Emailjs for the contact form
https://www.emailjs.com/

- You'll have to create an account to use its services.
- Then copy the 'config_test.js' file and rename it 'config.js'
- Enter your personal emailjs id in the variable

## Test with ecoindex
http://www.ecoindex.fr/resultats/?id=115238

- Note B : good but could be better for a single page like this one. I should work on my images format


## Accessibility test
https://wave.webaim.org/report#/https://portfolio-marie-guerin.surge.sh/ 


## Performance test
https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=https%3A%2F%2Fportfolio-marie-guerin.surge.sh%2F

- 65 % is not good enough, I have an issue with my PNG images, should be JPG and figure out how to use Parceljs correctly (minify)


## validator W3
https://validator.w3.org/nu/?doc=https%3A%2F%2Fportfolio-marie-guerin.surge.sh%2F

- One error for the preloader link, I'll fix it later









